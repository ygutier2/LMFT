# Landau Mean Field Theory

Here, you will find Mathematica notebooks with the instructions to generate interactive plots of the Free Energy Landscape (FEL) as function of the order parameter, for different classical systems (Ising model, Flory-Huggins model,etc). The codes compute the minima of the free energy and hopefully will help the user to compute and understand phase-diagrams of these systems.

##  Requirements
[Wolfram Mathematica 13](https://www.wolfram.com/mathematica/?source=nav) or above.

## To do
Write assignements so the user creates codes for more general FELs.